package juego;

import entorno.Entorno;

public abstract class Circulo extends ElementoDinamico {
	int diametro;

	Circulo(double x, double y, int diametro) {
		super(x, y);
		this.diametro = diametro;
	}

	public double getLimiteIzquierdo() {
		return this.x - (this.diametro / 2);
	}

	public double getLimiteDerecho() {
		return this.x + (this.diametro / 2);
	}

	public double getLimiteSuperior() {
		return this.y - (this.diametro / 2);
	}

	public double getLimiteInferior() {
		return this.y + (this.diametro / 2);
	}

	public void dibujarse(Entorno entorno) {
		entorno.dibujarCirculo(this.x, this.y, this.diametro, this.color);
	}

}