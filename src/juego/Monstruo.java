package juego;

import java.awt.Color;

public class Monstruo extends Circulo {
	Monstruo() {
		super(0, 0, 25);
		this.direccionHorizontal = EnumEstadosHorizontales.QUIETA;
		this.direccionVertical = EnumEstadosVerticales.QUIETA;
		this.setColor(Color.white);
	}
	
	public void mantenerseEnEntorno() {
		if (this.getLimiteSuperior() > 600) {
			this.viva = false;
		}
		if (this.getLimiteIzquierdo() == 0) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_DERECHA);
		}
		if (this.getLimiteDerecho() == 800) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_IZQUIERDA);
		}
	}

	void colisionoViga(Elemento elemento) {
		if (this.colisionHorizontal(elemento)) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.QUIETA);
		} else {
			this.setDireccionVertical(EnumEstadosVerticales.QUIETA);
		}

	}

	void colisionoMortal(Elemento elemento) {
		this.viva = false;
	}

	public void colisiono(Elemento elemento) {
		if (elemento instanceof Viga) {
			this.colisionoViga(elemento);
		} else if (elemento instanceof Congelante || elemento instanceof Mago) {
			this.colisionoMortal(elemento);
		}

	} 
} 