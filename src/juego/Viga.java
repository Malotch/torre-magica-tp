package juego;

import java.awt.Color;

public class Viga extends Cuadrado {

	Viga() {
		super(0, 0, 0, 0);

		this.setAncho(280);
		this.setAlto(20);
		this.setX(400);
		this.setY(100);
		this.setColor(Color.green);
	}

	Viga(int y) {
		super(0, 0, 0, 0);

		this.setAncho(280);
		this.setAlto(20);
		this.setX(400);
		this.setY(y);
		this.setColor(Color.green);
	}

	public void posicionIzquierda() {
		this.setX(120);
	}

	public void posicionDerecha() {
		this.setX(670);
	}
}
