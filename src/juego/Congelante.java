package juego;

import java.awt.Color;

import entorno.Entorno;

public class Congelante extends Circulo {

	Congelante(Mago mago) {
		super(mago.x, mago.y, 10);
	}

	void dibujar(Entorno entorno) {
		entorno.dibujarCirculo(this.x, this.y, this.diametro, Color.YELLOW);
	}

	public void colisiono(Elemento elemento) {
		if (this.colision(elemento)) {
			this.viva = false;
		}
	}

	void avanzar(Mago mago) {
		if (mago.getDireccionHorizontal()==EnumEstadosHorizontales.MOVER_IZQUIERDA) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_IZQUIERDA);
		}

		if (mago.getDireccionHorizontal()==EnumEstadosHorizontales.MOVER_DERECHA) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_DERECHA);
		}
		this.moverse();
	}
	
	public void mantenerseEnEntorno() {
		if	((this.getLimiteIzquierdo() == 0) || 
			(this.getLimiteDerecho() == 800) ) {
		this.viva = false;
		}		
	}
}