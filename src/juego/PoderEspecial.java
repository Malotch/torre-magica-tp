package juego;

import java.awt.Color;

import entorno.Entorno;

public class PoderEspecial extends Circulo {

	PoderEspecial(Mago exterminador) {
		super(exterminador.x, exterminador.y, 25);
		this.color = Color.ORANGE;
	}

	void dibujar(Entorno entorno) {
		entorno.dibujarCirculo(this.x, this.y, this.diametro, Color.YELLOW);
	}

	public void colisiono(Elemento elemento) {
		if (this.colision(elemento)) {
			if (elemento instanceof Monstruo || elemento instanceof Congelante) {
				this.explota();
			}
		}
	}

	public void explota() {
		this.viva = false;
	}
	
	public void mantenerseEnEntorno() {
		if	((this.getLimiteIzquierdo() == 0) || 
			(this.getLimiteDerecho() == 800) ) {
		this.viva = false;
		}		
	}

}