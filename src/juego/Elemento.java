package juego;

import java.awt.Color;
import entorno.Entorno;

public abstract class Elemento {
	protected boolean viva;
	protected double x;
	protected double y;
	protected Color color;

	public Elemento(double x, double y) {
		this.x = x;
		this.y = y;
		this.viva = true;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isViva() {
		return viva;
	}

	public void setViva(boolean viva) {
		this.viva = viva;
	}

	public abstract double getLimiteInferior();

	public abstract double getLimiteIzquierdo();

	public abstract double getLimiteDerecho();

	public abstract double getLimiteSuperior();

	public abstract void dibujarse(Entorno entorno);
}
