package juego;

public abstract class ElementoDinamico extends Elemento {

	protected EnumEstadosHorizontales direccionHorizontal;
	protected EnumEstadosVerticales direccionVertical;	
	public enum EnumEstadosHorizontales {
		MOVER_DERECHA, MOVER_IZQUIERDA, QUIETA
	}
	public enum EnumEstadosVerticales {
		MOVER_ARRIBA, MOVER_ABAJO, QUIETA
	}	

	ElementoDinamico(double x, double y) {
		super(x, y);
		this.direccionHorizontal = EnumEstadosHorizontales.QUIETA;
		this.direccionVertical = EnumEstadosVerticales.MOVER_ABAJO;
	}

	private void moverArriba() {
		this.y -= 1;
	}
	
	private void moverAbajo() {
		this.y += 1;
	}

	private void moverIzquierda() {
		this.x -= 1;
	}

	private void moverDerecha() {
		this.x += 1;
	}

	public EnumEstadosHorizontales getDireccionHorizontal() {
		return direccionHorizontal;
	}

	public void setDireccionHorizontal(EnumEstadosHorizontales direccionHorizontal) {
		this.direccionHorizontal = direccionHorizontal;
	}

	public EnumEstadosVerticales getDireccionVertical() {
		return direccionVertical;
	}

	public void setDireccionVertical(EnumEstadosVerticales direccionVerticales) {
		this.direccionVertical = direccionVerticales;
	}
	
	public void moverse() {
		if (this.direccionHorizontal == EnumEstadosHorizontales.MOVER_DERECHA) {
			this.moverDerecha();
		}
		if (this.direccionHorizontal == EnumEstadosHorizontales.MOVER_IZQUIERDA) {
			this.moverIzquierda();
		}
		if (this.direccionVertical == EnumEstadosVerticales.MOVER_ARRIBA) {
			this.moverArriba();
		}		
		if (this.direccionVertical == EnumEstadosVerticales.MOVER_ABAJO) {
			this.moverAbajo();
		}
	}
	
	public boolean colisionDerecha(Elemento elemento) {
		return this.getLimiteIzquierdo() < elemento.getLimiteDerecho();
	}

	public boolean colisionIzquierda(Elemento elemento) {
		return this.getLimiteDerecho() > elemento.getLimiteIzquierdo();
	}

	public boolean colisionSuperior(Elemento elemento) {
		return this.getLimiteSuperior() < elemento.getLimiteInferior();
	}

	public boolean colisionInferior(Elemento elemento) {
		return this.getLimiteInferior() > elemento.getLimiteSuperior();
	}

	public boolean colisionHorizontal(Elemento elemento) {
		return (this.colisionDerecha(elemento) && this.colisionIzquierda(elemento));
	}

	public boolean colisionVertical(Elemento elemento) {
		return (this.colisionInferior(elemento) && this.colisionSuperior(elemento));
	}

	public boolean colision(Elemento elemento) {
		return (this.colisionHorizontal(elemento)) && this.colisionVertical(elemento);
	}

	public abstract void colisiono(Elemento elemento);
	
	public abstract void mantenerseEnEntorno();
}