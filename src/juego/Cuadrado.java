package juego;

import java.awt.Color;

import entorno.Entorno;

public abstract class Cuadrado extends Elemento {
	protected int alto;
	protected int ancho;

	Cuadrado(double x, double y, int alto, int ancho) {
		super(x, y);
		this.alto = alto;
		this.ancho = ancho;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public double getLimiteInferior() {
		return this.y + (this.alto / 2);
	}

	public double getLimiteSuperior() {
		return this.y - (this.alto / 2);
	}

	public double getLimiteIzquierdo() {
		return this.x - (this.ancho / 2);
	}

	public double getLimiteDerecho() {
		return this.x + (this.ancho / 2);
	}

	public void dibujarse(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.GREEN);
	}

}