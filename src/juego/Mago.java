package juego;

import java.awt.Color;
import entorno.Entorno;

public class Mago extends ElementoDinamico {

	private double alto;
	private double ancho;
	private int vidas;

	Mago(int x, int y, int alto, int ancho) {
		super(x, y);
		this.alto = 30;
		this.ancho = 30;
		this.vidas = 3;
	}

	public void dibujarse(Entorno entorno) {
		entorno.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.BLUE);
	}

	void colisionoViga(Elemento elemento) {
		if (this.colision(elemento)) {
			if (this.colisionHorizontal(elemento)) {
				this.setDireccionHorizontal(EnumEstadosHorizontales.QUIETA);
			} else {
				this.setDireccionVertical(EnumEstadosVerticales.QUIETA);
			}
		}
	}

	void colisionoMortal(Elemento elemento) {
		if (this.colision(elemento)) {
			this.vidas--;
		}
		if (this.vidas == 0) {
			this.viva = false;
		}
	}

	public void colisiono(Elemento elemento) {
		if (this.colision(elemento)) {
			if (elemento instanceof Viga) {
				this.colisionoViga(elemento);
			} else if (elemento instanceof Monstruo) {
				this.colisionoMortal(elemento);
			}
		}
	}
	
	public void mantenerseEnEntorno() {
		if (this.getLimiteSuperior() > 600) {
			this.setY(-10);
		}
		if (this.getLimiteIzquierdo() == 0) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_DERECHA);
		}
		if (this.getLimiteDerecho() == 800) {
			this.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_IZQUIERDA);
		}
	}

	public double getLimiteInferior() {
		return this.y + (this.alto / 2);
	}

	public double getLimiteIzquierdo() {
		return this.x - (this.ancho / 2);
	}

	public double getLimiteDerecho() {
		return this.x + (this.ancho / 2);
	}

	public double getLimiteSuperior() {
		return this.y - (this.alto / 2);
	}

}
