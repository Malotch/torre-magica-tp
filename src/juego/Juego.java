package juego;

import java.awt.Color;

import entorno.Entorno;
import entorno.InterfaceJuego;
import juego.ElementoDinamico.EnumEstadosHorizontales;
import java.util.ArrayList;
import java.util.Random;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private estadosDeJuego estadoDeJuego;
	int puntuacion;
	int cantidadDeVigas;
	int intSpawnMonstruo;
	Random random = new Random(System.currentTimeMillis());
	Mago mago = null;
	ArrayList<Elemento> elementos = new ArrayList<Elemento>();
	int contadorTicks = 0;

	// Variables y mÃ©todos propios de cada grupo
	// ...
	Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Juego Torre Magica", 800, 600);
		this.estadoInicial();
	}

	public void estadoInicial() {
		estadoDeJuego = estadosDeJuego.JUGANDO;
		puntuacion = 0;
		mago = new Mago(400, 70, 50, 50);
		cantidadDeVigas = 7;
		for (int i = 0; i < cantidadDeVigas; i++) {
			if (i != 3 && i != 4) {
				elementos.add(new Viga());
				if (i == 1 || i == 5) {
					((Viga) elementos.get(i)).posicionIzquierda();
				} else if (i == 2 || i == 6) {
					((Viga) elementos.get(i)).posicionDerecha();
				}
				if (i == 1 || i == 2) {
					((Viga) elementos.get(i)).setY(200);
				} else if (i == 5 || i == 6) {
					((Viga) elementos.get(i)).setY(600);
				}
			}
			if (i == 3) {
				elementos.add(new Viga(350));
			}
			if (i == 4) {
				elementos.add(new Viga(450));
				((Viga) elementos.get(i)).setAncho(400);
			}
		}

		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el mÃ©todo tick() serÃ¡ ejecutado en cada instante y por lo
	 * tanto es el mÃ©todo mÃ¡s importante de esta clase. AquÃ­ se debe actualizar
	 * el estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		switch (this.estadoDeJuego) {

		case JUGANDO:
			this.movimientoMago();
			this.dibujarElementos();
			this.eliminarMuertos();
			contadorTicks++;
			break;

		case GANADO:

			// festejoMago
			entorno.cambiarFont("Font.PLAIN", 20, Color.BLUE);
			entorno.escribirTexto("GANASTE", 300, 300);
			entorno.escribirTexto("Puntuacion: " + puntuacion, 550, 50);
			break;

		case PERDIDO:

			// festejoMonstruo
			entorno.cambiarFont("Font.PLAIN", 20, Color.GREEN);
			entorno.escribirTexto("PERDISTE. apreta enter para volver a arrancar", 200, 300);
			entorno.escribirTexto("Puntuacion: " + puntuacion, 550, 50);
			if (entorno.estaPresionada(entorno.TECLA_ENTER)) {
				this.estadoInicial();
			}
			break;
		}
		// Procesamiento de un instante de tiempo
		// ...
		// Actualizacion deljueg

	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

	public enum estadosDeJuego {
		JUGANDO, GANADO, PERDIDO
	}

	public estadosDeJuego getestadosDeJuego() {
		return estadoDeJuego;
	}

	public void dibujarElementos() {
		mago.dibujarse(entorno);
		entorno.escribirTexto("Puntuacion: " + puntuacion, 700, 50);
		entorno.escribirTexto("Ticks " + contadorTicks, 400, 300);
		for (int i = 0; i < elementos.size(); i++) {
			elementos.get(i).dibujarse(entorno);
		}
	}

	public void movimientoMago() {
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			mago.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_DERECHA);
		}
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			mago.setDireccionHorizontal(EnumEstadosHorizontales.MOVER_IZQUIERDA);
		}		
		for (int i = 0; i < elementos.size(); i++) {
			mago.colisiono(elementos.get(i));
		}
		mago.mantenerseEnEntorno();	
		mago.moverse();

	}

	/*
	 * public void armasExterminador() { if
	 * (entorno.estaPresionada(entorno.TECLA_ESPACIO) && contadorTicks % 15 == 0) {
	 * elementos.add(new Disparo(mago)); } for (int i = 0; i < elementos.size();
	 * i++) { if (elementos.get(i) instanceof Disparo) { Disparo disparo = (Disparo)
	 * elementos.get(i); disparo.avanzar(); if (disparo.getX() > 800 ||
	 * disparo.getX() < 0 || disparo.getY() < 0 || disparo.getY() > 600 ) {
	 * disparo.setViva(false); } }
	 * 
	 * } for (int i = 0 ; i < elementos.size(); i++) { if (elementos.get(i)
	 * instanceof Disparo) { Disparo disparo = (Disparo) elementos.get(i); for (int
	 * j = 0; j < elementos.size(); j++) { if (elementos.get(j) instanceof Monstruo
	 * || elementos.get(j) instanceof Mina) { if
	 * (disparo.colision(elementos.get(j))) { disparo.colisiono(elementos.get(j)); }
	 * }
	 * 
	 * } } }
	 * 
	 * }
	 */

	public void eliminarMuertos() {
		for (int i = 0; i < elementos.size(); i++) {
			if (elementos.get(i).isViva() == false) {
				elementos.remove(i);
			}
		}
	}
}
